class AdministrationsController < ApplicationController
  before_action :authenticate_user!

  def show
    if current_user.admin?
      users = User.all
      render json: Administration.add_parameters(users)
    else
      render body: nil, status: 505
    end
  end

  def xlsx_application
    xlsx = Administration.application_xlsx params[:users]
    send_data xlsx
  end

  def users
    date_min = Date.parse params[:dateMin]
    date_max = Date.parse(params[:dateMax]) + 2
    users = User.where(created_at: date_min...date_max)
    render json: Administration.add_parameters(users)
  end
end
