require 'axlsx'
class Administration < SponlineRecord
  def self.application_xlsx(users)
    p = Axlsx::Package.new
    wb = p.workbook
    align_center = wb.styles.add_style(alignment: { horizontal: :center }, border: Axlsx::STYLE_THIN_BORDER)
    align_left = wb.styles.add_style(alignment: { horizontal: :left }, b: true)
    align_center_bold_header = wb.styles.add_style(alignment: { horizontal: :center, vertical: :center }, border: Axlsx::STYLE_THIN_BORDER, sz: 30)
    align_center_bold = wb.styles.add_style(alignment: { horizontal: :center }, b: true, border: Axlsx::STYLE_THIN_BORDER)
    ws = wb.add_worksheet(name: 'Пользователи') do |sheet|
      sheet.merge_cells 'A1:G1'
      sheet.merge_cells 'A2:G6'
      sheet.merge_cells 'A7:G7'
      sheet.merge_cells 'A8:G8'
      sheet.merge_cells 'A9:G9'
      sheet.add_row ['']
      sheet.add_row ['Список пользователей сервиса СПРОЕКТИРУЙ.РФ'], style: align_center_bold_header
      sheet.add_row ['']
      sheet.add_row ['']
      sheet.add_row ['']
      sheet.add_row [''], style: align_center_bold_header
      sheet.add_row ['']
      sheet.add_row ["Количество пользователей всего: #{users.count}"], style: align_left
      sheet.add_row ['']
      sheet.add_row ['N', 'Дата регистрации', 'Город', 'Область', 'Email', 'Имя', 'Количество расчетов'], style: align_center_bold
      users.sort_by! { |user| user[:created_at] }
      users.each_with_index { |user, index| sheet.add_row [index + 1, user[:created_at], user[:city], user[:region], user[:email], user[:name], user[:count_of_docs]], style: align_center } if users
    end
    ws.column_widths 10, 20, 20, 40, 30, 20
    p.to_stream.read
  end

  def self.add_parameters(users)
    users = users.as_json
    users.each do |user|
      user["created_at"] = user["created_at"].strftime("%Y-%m-%d %H:%M:%S")
      user["city"] = City.find_by_id(user["city_id"]).full_name if City.find_by_id(user["city_id"])
      user["region_id"] = City.find_by_id(user["city_id"]).region_id if City.find_by_id(user["city_id"])
      user["region"] = Region.find_by_id(user["region_id"]).full_name if City.find_by_id(user["region_id"])
      user["count_of_docs"] = Document.all.where(user_id: user["id"]).count
    end
    users
  end
end